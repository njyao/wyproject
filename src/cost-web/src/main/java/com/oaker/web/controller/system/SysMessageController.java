package com.oaker.web.controller.system;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.oaker.common.annotation.Log;
import com.oaker.common.core.controller.BaseController;
import com.oaker.common.core.domain.AjaxResult;
import com.oaker.common.enums.BusinessType;
import com.oaker.system.domain.SysMessage;
import com.oaker.system.service.ISysMessageService;
import com.oaker.common.utils.poi.ExcelUtil;
import com.oaker.common.core.page.TableDataInfo;

/**
 * 消息通知
 * 
 * @author ruoyi
 * @date 2023-11-23
 */
@RestController
@RequestMapping("/system/message")
public class SysMessageController extends BaseController
{
    @Autowired
    private ISysMessageService sysMessageService;

    /**
     * 查询消息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysMessage sysMessage)
    {
        startPage();
        System.out.println("list:"+sysMessage);
        List<SysMessage> list = sysMessageService.selectSysMessageList(sysMessage);
        return getDataTable(list);
    }

    @GetMapping("/unreadlist")
    public TableDataInfo unreadlist(SysMessage sysMessage)
    {
        startPage();
        sysMessage.setStatus(0);
        System.out.println("unreadlist:"+sysMessage);
        List<SysMessage> list = sysMessageService.selectSysMessageList(sysMessage);
        return getDataTable(list);
    }


    /**
     * 查询未读数量
     */
    @GetMapping("/count")
    public AjaxResult count()
    {
        return toAjax(0);
    }


    /**
     * 导出消息列表
     */
    @Log(title = "消息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysMessage sysMessage)
    {
        List<SysMessage> list = sysMessageService.selectSysMessageList(sysMessage);
        ExcelUtil<SysMessage> util = new ExcelUtil<SysMessage>(SysMessage.class);
        try {
            util.exportExcel(response, list, "消息数据");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取消息详细信息
     */
    @GetMapping(value = "/{messageId}")
    public AjaxResult getInfo(@PathVariable("messageId") Integer messageId)
    {
        return  AjaxResult.success(sysMessageService.selectSysMessageByMessageId(messageId));
    }

    /**
     * 新增消息
     */
    @PreAuthorize("@ss.hasPermi('system:message:add')")
    @Log(title = "消息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysMessage sysMessage)
    {
        return toAjax(sysMessageService.insertSysMessage(sysMessage));
    }

    /**
     * 修改消息
     */
    @PreAuthorize("@ss.hasPermi('system:message:edit')")
    @Log(title = "消息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysMessage sysMessage)
    {
        return toAjax(sysMessageService.updateSysMessage(sysMessage));
    }

    /**
     * 删除消息
     */
    @Log(title = "消息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{messageIds}")
    public AjaxResult remove(@PathVariable Integer[] messageIds)
    {
        return toAjax(sysMessageService.deleteSysMessageByMessageIds(messageIds));
    }

    /**
     * 设置为已读
     */
    @Log(title = "消息", businessType = BusinessType.UPDATE)
    @PostMapping("/read/{messageIds}")
    public AjaxResult read(@PathVariable Integer[] messageIds)
    {
        return toAjax(sysMessageService.readSysMessageByMessageIds(messageIds));
    }
}
